# model-to-json-schema

A simple artisan command to Generate a json-schema representation of a given Eloquent model to be used in AWS Api Gateway.

Note that the schema generated should be seen more as a boilerplate.

The schema is built by retrieving the first row from the table and using the `getAttributes` method on the model. If any relations are loaded via the `protected $with` property on the model, they will be included. If the table is empty, the command falls back to using the columns in the table. You can force using the table by using the `-t` option. 

If you would like to generate a schema for a collection (index response) you can use the `-c` option. If a model schema already exists for the collection, you can specify the `-r` option to generate a reference to the model. Please note that you have to replace `{appId}` in the generated schema with your app id.

## Install 
Copy the ModelToJsonSchema.php file into app/Console/Commands. 
## Usage
`php artisan json-schema {Model}`

### Examples

In the following examples the users table has a `password` and `remember_token` field which is in the `$hidden` array on the model. 
There is also a `some_appended_attribute` in the `$appends` array together with a `getSomeAppendedAttribute` method that returns a `double`.

`php artisan json-schema User`

```
{
    "type": "object",
    "$schema": "http://json-schema.org/draft-04/schema#",
    "definitions": {},
    "title": "The User schema",
    "required": [
        "id",
        "name",
        "email",
        "email_verified_at",
        "last_login_at",
        "created_at",
        "updated_at",
        "an_appended_attribute",
        "roles"
    ],
    "properties": {
        "id": {
            "type": "integer"
        },
        "name": {
            "type": "string"
        },
        "email": {
            "type": "string"
        },
        "email_verified_at": {
            "type": "string"
        },
        "last_login_at": {
            "type": "string"
        },
        "created_at": {
            "type": "string"
        },
        "updated_at": {
            "type": "string"
        },
        "some_appended_attribute": {
            "type": "number"
        },
        "roles": {
            "type": "array",
            "items": {
                "type": "object",
                "title": "The User_roles Schema",
                "required": [
                    "name"
                ],
                "properties": {
                    "name": {
                        "type": "string"
                    }
                }
            }
        }
    }
}
```

`php artisan json-schema User -t`

```
{
    "type": "object",
    "$schema": "http://json-schema.org/draft-04/schema#",
    "definitions": {},
    "title": "The User schema",
    "required": [
        "id",
        "name",
        "email",
        "email_verified_at",
        "password",
        "last_login_at",
        "remember_token",
        "created_at",
        "updated_at"
    ],
    "properties": {
        "id": {
            "type": "integer"
        },
        "name": {
            "type": "string"
        },
        "email": {
            "type": "string"
        },
        "email_verified_at": {
            "type": "string"
        },
        "password": {
            "type": "string"
        },
        "last_login_at": {
            "type": "string"
        },
        "remember_token": {
            "type": "string"
        },
        "created_at": {
            "type": "string"
        },
        "updated_at": {
            "type": "string"
        }
    }
}
```
`php artisan json-schema User -c`

```
{
    "type": "object",
    "$schema": "http://json-schema.org/draft-04/schema#",
    "definitions": {},
    "title": "The Users Schema",
    "required": [
        "users"
    ],
    "properties": {
        "users": {
            "type": "array",
            "items": {
                "type": "object",
                "required": [
                    "id",
                    "is_admin",
                    "name",
                    "email",
                    "email_verified_at",
                    "last_login_at",
                    "created_at",
                    "updated_at"
                ],
                "properties": {
                    "id": {
                        "type": "integer"
                    },
                    "is_admin": {
                        "type": "integer"
                    },
                    "name": {
                        "type": "string"
                    },
                    "email": {
                        "type": "string"
                    },
                    "email_verified_at": {
                        "type": "string"
                    },
                    "last_login_at": {
                        "type": "string"
                    },
                    "created_at": {
                        "type": "string"
                    },
                    "updated_at": {
                        "type": "string"
                    }
                }
            }
        }
    }
}

```

`php artisan json-schema User -cr`
```
{
    "type": "object",
    "$schema": "http://json-schema.org/draft-04/schema#",
    "definitions": {},
    "title": "The Users Schema",
    "required": [
        "users"
    ],
    "properties": {
        "users": {
            "type": "array",
            "items": {
                "$ref": "https://apigateway.amazonaws.com/restapis/{appId}/models/User"
            }
        }
    }
}
```
