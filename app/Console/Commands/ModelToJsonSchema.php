<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\DetectsApplicationNamespace;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class GenerateModelJsonSchema
 * @package App\Console\Commands
 */
class ModelToJsonSchema extends Command
{
    use DetectsApplicationNamespace;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'json-schema
    {Model : The model name. Namespace defaults to application namespace. For other namespaces use \'My\NameSpace\User\' or My\\\\NameSpace\\\\User.}
    {--t|table : Force using the table columns instead of model attributes.}
    {--c|collection : Generate for a collection response.}
    {--r|reference : If you already have a defined model, use model reference for collection.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generate json-schema representation of a given model";


    /**
     * The full classname of the given model.
     *
     * @var string
     */
    protected $class = '';

    /**
     * The classname of the given model without any namespaces.
     *
     * @var string
     */
    protected $modelName = '';

    /**
     * Instance of given model used to build queries.
     *
     * @var Model
     */
    protected $model;

    /**
     * The first model from the database.
     *
     * @var Model
     */
    protected $instance;

    /**
     * The array used to build the schema.
     *
     * @var array
     */
    protected $schema;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*
         * Handle input
         */
        $this->class = $this->argument('Model');
        if (str_contains($this->class, "\\") === false) {
            // If no namespace is given, prepend the default namespace
            // and set the model name to the argument provided.
            $namespace = $this->getAppNamespace();
            $this->class = "\\{$namespace}{$this->class}";
            $this->modelName = $this->argument('Model');
        } else {
            // If the namespace is provided, parse it to ensure that it is being
            // referenced from the base namespace and extract the model name.
            $this->class = "\\" . ltrim($this->class, "\\");
            $this->modelName = array_last(explode('\\', $this->class));
        }

        /*
         * Create an empty instance of the given class
         * and validate that it is an eloquent model.
         */
        try {
            $this->model = app()->make($this->class);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            return;
        }
        if ($this->valueIsModel($this->model) === false) {
            $this->error("Class {$this->class} must extend the abstract \Illuminate\Database\Eloquent\Model class.");
            return;
        }

        /*
         * Set the basic schema properties.
         */
        $this->schema = $this->freshSchema($this->modelName);/*
         * First check if there is any entries in the DB so we can extract the
         * attributes as well as any relations that are loaded by default. If
         * the table is empty, we fall back to using the table columns.
         */;
        if ($this->option('table') === false && $this->instance = $this->model->query()->first()) {
            $this->setSchemaFromModel();
        } else {
            $this->setSchemaFromTable();
        }
        $this->showOutput();
    }

    /**
     * Generate the schema by using the 'getAttributes' method on a fresh instance of
     * the model. This method also generates inner schemas for preloaded relations
     * set in the $with array property on the model.
     */
    protected function setSchemaFromModel()
    {
        $instance = $this->instance;
        $data = method_exists('toJsonSchema', $instance) ? $instance->toJsonSchema() : $instance->toArray();
        $relationNames = array_keys($instance->getRelations());

        foreach ($data as $key => $val) {
            $type = "string";
            if (is_numeric($val)) {
                $type = is_int($val) ? "integer" : "number";
            } elseif (is_object($val) || is_array($val)) {
                $type = "object";
            }
            $this->schema['required'][] = $key;
            $this->schema['properties'][$key] = ["type" => "{$type}"];

            if (is_array($val)) {
                if (in_array($key, $relationNames)) {
                    $val = $instance->{$key};
                    /*
                     * Check if the relationship is a collection (a one-to-many/many-to-many relationship).
                     * If it is, we grab the first one out else we get a fresh instance from the database.
                     */
                    if ($this->valueIsCollection($val)) {
                        $val = $val->first();
                        if ($val) {
                            $val = $val->fresh();
                        }
                    } elseif ($this->valueIsModel($val)) {
                        // fine
                        $val = $val->fresh();
                    }
                    $val = $val->toArray();
                }
                $innerSchema = array_merge($this->schema['properties'][$key], [
                    'title' => "The {$this->modelName}_{$key} Schema",
                    'required' => [],
                    'properties' => [],
                ]);

                foreach ($val as $k => $v) {
                    $t = "string";
                    if (is_numeric($v)) {
                        $t = is_int($v) ? "integer" : "number";
                    } elseif (is_object($v) || is_array($v)) {
                        $t = "object";
                    }

                    $innerSchema['properties'][$k] = ["type" => "{$t}"];
                    $innerSchema['required'][] = $k;
                }

                if ($this->valueIsCollection($instance->{$key})) {
                    $obj = $innerSchema;
                    $innerSchema = [];
                    $innerSchema['type'] = "array";
                    $innerSchema['items'] = $obj;
                }
                $this->schema['properties'][$key] = $innerSchema;
            }
        }
    }

    /**
     * Generate the schema by using the table columns of the given model.
     */
    protected function setSchemaFromTable()
    {
        $tableName = $this->model->getTable();
        $columns = DB::select("SHOW COLUMNS IN {$tableName}");
        foreach ($columns as $col) {
            $type = "string";
            if (str_contains($col->Type, "int")) {
                $type = "integer";
            } elseif (str_contains($col->Type, "double")) {
                $type = "number";
            }

            $this->schema['properties'][$col->Field] = ['type' => $type];
            $this->schema['required'][] = $col->Field;
        };
    }

    /**
     * Print out the json encoded $schema the output. We first check to see if python is
     * installed and pipe it through the json.tool utility for prettier output if it is.
     */
    protected function showOutput()
    {

        $pretty = false;
        $schema = $this->schema;

        if ($this->option('collection')) {
            $s = snake_case(str_plural($this->modelName));
            $schema['properties'] = [$s => $schema];
            $schema['title'] = "The " . str_plural($this->modelName) . " Schema";
            $schema['required'] = [$s];
            $schema['properties'][$s]['type'] = "array";
            if ($this->option('reference')) {
                $schema['properties'][$s]['items'] = [
                    '$ref' => "https://apigateway.amazonaws.com/restapis/{appId}/models/{$this->modelName}"
                ];

            } else {
                $schema['properties'][$s]['items'] = [
                    'type' => "object",
                    'required' => $schema['properties'][$s]['required'],
                    'properties' => $schema['properties'][$s]['properties'],

                ];
            }
            unset($schema['properties'][$s]['$schema']);
            unset($schema['properties'][$s]['title']);
            unset($schema['properties'][$s]['definitions']);

            unset($schema['properties'][$s]['properties']);
            unset($schema['properties'][$s]['required']);
        }

        $pythonPath = trim(`echo $(which python3)`);
        if (empty($pythonPath))
            $pythonPath = trim(`echo $(which python)`);
        if (!empty($pythonPath)) {
            $pretty = true;
        } else {
            $pretty = false;
        }
        $output = str_replace([
            'http:\/\/json-schema.org\/draft-04\/schema#',
            "https:\/\/apigateway.amazonaws.com\/restapis\/{appId}\/models\/"
        ], [
            'http://json-schema.org/draft-04/schema#',
            "https://apigateway.amazonaws.com/restapis/{appId}/models/"
        ],
            json_encode($schema)
        );

        if ($pretty) {
            $output = `echo '{$output}' | $pythonPath -m json.tool`;
        }
        $this->info($output);
    }

    /**
     * The basic properties of the schema.
     *
     * @param string $class
     * @return array
     */
    protected function freshSchema(string $class): array
    {
        return [
            "type" => "object",
            "\$schema" => "http://json-schema.org/draft-04/schema#",
            "definitions" => new \stdClass,
            "title" => "The {$class} schema",
            "required" => [],
            "properties" => [],
        ];
    }


    /**
     * @param $val
     * @return bool
     */
    protected function valueIsCollection($val): bool
    {
        return $val instanceof \Illuminate\Support\Collection;
    }

    /**
     * @param $val
     * @return bool
     */
    protected function valueIsModel($val): bool
    {
        return $val instanceof \Illuminate\Database\Eloquent\Model;
    }
}
